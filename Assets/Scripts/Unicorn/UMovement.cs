﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class UMovement : MonoBehaviour
{

    public KeyCode MoveLeft;
    public KeyCode MoveRight;

    public float changeVel = 0;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(changeVel, 0, 1);

        if(Input.GetKeyDown(MoveLeft))
        {
            changeVel = -8;
            StartCoroutine (Stopchange());
            
        }

        if (Input.GetKeyDown(MoveRight))
        {
            GetComponent<Rigidbody>().position = new Vector3(0, 1, );
        }
        

    }

    IEnumerator Stopchange()
    {
        yield return new WaitForSeconds(.125f);
        changeVel = 0;

    }
}
